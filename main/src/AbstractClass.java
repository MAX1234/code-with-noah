public abstract class AbstractClass {
    public abstract float getDamage();

    public void print() {
        System.out.printf("Damage: %f\n", this.getDamage());
    }
}
