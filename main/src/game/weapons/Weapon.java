package game.weapons;

public abstract class Weapon {
    public abstract float getDamage();
    public abstract String getName();

    public void print() {
        System.out.printf("%s is a weapon with %f damage", this.getName(), this.getDamage());
    }

//    public void damage() {
//
//    }

    // TODO: implement damage
}
