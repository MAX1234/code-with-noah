public class Tesla extends Car {
    private int charges = 0;

    public Tesla(String name, String model, int chargs) {
        super(name, "Tesla", model);
        this.charges = chargs;
    }

    public void charge(int amount) {
        System.out.printf("Charging by %d!\n", amount);
        this.charges += amount;
    }

    @Override
    public void print() {
        super.print();
        System.out.printf("And %s has %d charges.\n", this.name, this.charges);
    }

    @Override
    public void go(int miles) {
        if (this.charges > 0) {
            super.go(miles);
            this.charges -= miles;
        } else {
            System.out.println("No charges remaining!");
        }
    }
}
