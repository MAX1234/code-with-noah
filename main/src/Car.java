public class Car {
    public String name;
    public String company;
    public String model;
    protected int milesGone;

    public Car(String name, String company, String model) {
        this.name = name;
        this.company = company;
        this.model = model;
        this.milesGone = 0;
    }

    public void print() {
        System.out.printf("%s is a %s %s that has gone %d miles.\n", this.name, this.company, this.model, milesGone);
    }

    public void go(int miles) {
        this.milesGone += miles;
    }
}
