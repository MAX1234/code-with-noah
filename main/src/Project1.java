import java.util.*;
import java.util.regex.Pattern;

public class Project1 {
    public static void main(String[] args) {
        System.out.println("\27[41m");

        int number = 0;
        number = number + 2;
        System.out.print(number);
        System.out.print(number);
        System.out.println();
        System.out.println(number);
        System.out.println(number);
        System.out.printf("Number = %d\n", number);

        float a = 0.1f;

        System.out.println(a);

        char b = 'a';
        // 'abc'
        for (; b <= 'z'; b++) {
            System.out.print(b);
        }

        System.out.println();


        // b = 'a', number = 2
        char ch = 'c';
        String c = "a" + ch + number;
        // "a" + 'c' + 2 = "a" + "c" + "2" = "ac2"

        // c = "ac2"
        System.out.println(c);

        int x = 1 / 2;

        System.out.println(x);

        float y = 1 / 2;
        System.out.println(y);

        double z = 1f / 2.0;
        System.out.println(z);

        int[] p = new int[10];
        for (int i = 0; i < 10; i++) {
            p[i] = 10 - i;
        }

        System.out.println(Arrays.toString(p));

        coolness(0, 1);
        coolness(0, 1);

        int temp = add(1, 2);
        System.out.println(temp);

//        recursiveFunction(-1);
        // 10000000000000000000000000000001 -> 11111111111111111111111111111111
        // 01111111111111111111111111111111 -> 00000000000000000000000000000000

        System.out.println(fibonacci(10));
        System.out.println(lucas(10));
        System.out.println(tribonacci(10));

//        int[] array = new int[10];
        // (1) Multiple items? Yes
        // (2) Random access? Yes
        // System.out.println(array[2]);
        // (3) Unbounded? No
//        for (int i = 0; i < 1_000_000; i++) {
//            array[i] = 0;
//        }

        List<Integer> list = new ArrayList<>(10);
        // (1) Multiple items? Yes
        list.add(1);
        list.add(2);
        list.add(3);
        // (2) Random access? Yes
//        System.out.println(list.get(0));
//        System.out.println(list.get(1));
//        System.out.println(list.get(2));
        // (3) Unbounded? Yes
//        for (int i = 0; i <= 1_000_000; i++) {
//            list.add(i);
//        }

//        list.remove(1_000_000);
//        System.out.println(list.size());
//
//        Set<Integer> set = new HashSet<>();
        // (1) Multiple items? Yes
//        set.add(1);
//        set.add(2);
//        set.add(3);
        // (2) Random access? No
//        System.out.println(set.get(0));
//        System.out.println(set.get(1));
//        System.out.println(set.get(2));
        // (3) Unbounded? Yes
//        for (int i = 0; i <= 1_000_000; i++) {
//            set.add(i);
//        }

        // Set requirements
        // (1) Multiple items? Yes ==> Iterable
        // (2) Random access? NO, because it is unordered
        // (3) Unbounded? Yes

//        Iterable<Integer> iter = list;
//        Iterable<Integer> iter2 = set;

//        for (Integer integer : iter) {
//            System.out.println(integer);
//        }
//
//        for (Integer integer : iter2) {
//            System.out.println(integer);
//        }

//        Map<Integer, Integer> map = new HashMap<>();
//        map.put(1, 3);
//        map.put(5, 3);
//        map.put(6, 3);
//        Iterable iter3 = map.entrySet();
//
//        for (var value : iter3) {
//            System.out.println(value);
//        }
//        Iterable iter3 = map;

        collatz(10);

        for (int i = 0; i < 21; i++) {
            System.out.println(Math.pow(2, i)); // 2 ^ i
        }

        System.out.println("Lesson 3: Classes");

        Car car = new Car("Speedster", "Toyota", "Highlander");
        for(int i = 0; i < 100; i++) {
            car.go((int)(Math.random() * 100));
            car.print();
        }

        Tesla tesla = new Tesla("Firebeast", "Model X", 0);
        tesla.go(10);
        for(int q = 0; q < 3; q++) {
            tesla.charge(10);
            for (int i = 0; i < 10; i++) {
                tesla.go(1);
                tesla.print();
            }
            tesla.go(1);
        }

        Sword sword = new Sword();
        sword.print();
    }

    public static void coolness(int a, float b) {
        System.out.println(a);
        System.out.println(b);
    }

    public static int add(int a, int b) {
        return a + b;
    }

    static int counter = 0;

    public static void recursiveFunction(int a) {
        if (a == 0) {
            return;
        }

        counter++;
        System.out.println("Run " + counter + " times.");

        recursiveFunction(a - 1);
    }

    public static int fibonacci(int n) {
        if (n == 0 || n == 1) {
            return 1;
        }

        return fibonacci(n - 1) + fibonacci(n - 2);
        // 1 -> 2 -> 4
        // 2^n
    }

    public static int lucas(int n) {
        if (n == 0) {
            return 1;
        } else if (n == 1) {
            return 3;
        }

        return lucas(n - 1) + lucas(n - 2);
        // 1 -> 2 -> 4
        // 2^n
    }

    public static int tribonacci(int n) {
        if (n == 0 || n == 1 || n == 2) {
            return 1;
        }

        return tribonacci(n - 1) + tribonacci(n - 2) + tribonacci(n - 3);
        // 1 -> 2 -> 4
        // 2^n
    }

    public static void collatz(int n) {
        System.out.println(n);
        if (n == 1) {
            return;
        }
        if (n % 2 == 0) {
            collatz(n / 2);
        } else {
            collatz(3 * n + 1);
        }
    }

    /*
     * public Car makeCar(Steel steel, Electronics e, Motor m, Parts s) {
     *
     * }
     */
}
